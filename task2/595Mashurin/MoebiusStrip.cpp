#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <LightInfo.hpp>
#include <Texture.hpp>

#include <iostream>
#include <algorithm>

/*
 Вариант: анимация текстуры
 Добавить освещение с бликами.
 Наложить на поверхность две текстуры с разными текстурными координатами.
 Сделать, чтобы текстурные координаты изменялись во времени с некоторым периодом. Т.е. текстуры должны как-бы <<двигаться>> по модели.
 */

class MoebiusStrip : public Application {
public:
	MoebiusStrip() : _count(300), _lr(3.0), _phi(0.0), _theta(glm::pi<float>() * 0.25f) {}

    void makeScene() override {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _make_mobius_strip();
        _moebius_strip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _marker = makeSphere(0.1f);

        _shader = std::make_shared<ShaderProgram>("595MashurinData2/texture.vert", "595MashurinData2/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("595MashurinData2/marker.vert", "595MashurinData2/marker.frag");

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        _Texture1 = loadTexture("595MashurinData2/fire.jpg");
        _Texture2 = loadTexture("595MashurinData2/water.png");

        glGenSamplers(1, &_sampler1);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_REPEAT);
        
        glGenSamplers(1, &_sampler2);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void update() override {
        Application::update();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _moebius_strip->modelMatrix());
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
        
        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        float time = glfwGetTime();
        float diff_time = (cos(time) + 1) / 2;
        _shader->setFloatUniform("time", time/3);
        _shader->setFloatUniform("diff", diff_time);
        
        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler1);
        _Texture1->bind();

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler2);
        _Texture2->bind();

        _shader->setIntUniform("diffuseTex", 0);
        _shader->setIntUniform("diffuseTex2", 1);
        _shader->setMat4Uniform("modelMatrix", _moebius_strip->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _moebius_strip->modelMatrix()))));
            
        _moebius_strip->draw();

        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();
    }

	void handleKey(int key, int scancode, int action, int mods) override {
		Application::handleKey(key, scancode, action, mods);

		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_MINUS) {
				_count -= 10;
				if (_count)
				    makeScene();
			}
			if (key == GLFW_KEY_EQUAL) {
				_count += 10;
				if (_count)
				    makeScene();
			}
		}
	}

private:
    float _vertex_x( float u, float v ) {
        return cos(v) * (1 + u * cos(v / 2));
    }

    float _vertex_y( float u, float v ) {
        return sin(v) * (1 + u * cos(v / 2));
    }

    float _vertex_z( float u, float v ) {
        return u * sin(v / 2);
    }

    float _normal_x( float u, float v ) {
        return 2 * sin(v / 2) * (cos(v) - u * sin(v / 2) * sin(v));
    }

    float _normal_y( float u, float v ) {
        return 2 * sin(v / 2) * sin(v) + u * (cos(v) + sin(v) * sin(v));
    }

    float _normal_z( float u, float v ) {
        return -2 * cos(v / 2) * (1 + u * cos(v / 2));
    }

    void _make_mobius_strip() {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;

        float pi = (float)glm::pi<float>();

        for (unsigned int i = 0; i < _count; i++) {
            float u = -0.4f + 0.4f / _count * i;
            float u1 = -0.4f + 0.4f / _count * (i + 1);

            for (unsigned int j = 0; j < _count; j++) {
                float v =  2.0f * pi / _count * j;
                float v1 = 2.0f * pi / _count * (j + 1);

                for (int k = 0; k < 3; k += 2) {
                    vertices.push_back(glm::vec3(_vertex_x(u, v + k * pi), _vertex_y(u, v + k * pi), _vertex_z(u, v + k * pi)));
                    vertices.push_back(glm::vec3(_vertex_x(u1, v1 + k * pi), _vertex_y(u1, v1 + k * pi), _vertex_z(u1, v1 + k * pi)));
                    vertices.push_back(glm::vec3(_vertex_x(u1, v + k * pi), _vertex_y(u1, v + k * pi), _vertex_z(u1, v + k * pi)));

                    normals.push_back(glm::normalize(glm::vec3(_normal_x(u, v), _normal_y(u, v), _normal_z(u, v))));
                    normals.push_back(glm::normalize(glm::vec3(_normal_x(u1, v1), _normal_y(u1, v1), _normal_z(u1, v1))));
                    normals.push_back(glm::normalize(glm::vec3(_normal_x(u1, v), _normal_y(u1, v), _normal_z(u1, v))));

                    texcoords.push_back(glm::vec2((float)j / _count, 1.0f - (float)i / _count));
                    texcoords.push_back(glm::vec2((float)(j + 1) / _count, 1.0f - (float)(i + 1) / _count));
                    texcoords.push_back(glm::vec2((float)(j + 1) / _count, 1.0f - (float)i / _count));
                }

                for (int k = 0; k < 3; k += 2) {
                    vertices.push_back(glm::vec3(_vertex_x(u, v + k * pi), _vertex_y(u, v + k * pi), _vertex_z(u, v + k * pi)));
                    vertices.push_back(glm::vec3(_vertex_x(u, v1 + k * pi), _vertex_y(u, v1 + k * pi), _vertex_z(u, v1 + k * pi)));
                    vertices.push_back(glm::vec3(_vertex_x(u1, v1 + k * pi), _vertex_y(u1, v1 + k * pi), _vertex_z(u1, v1 + k * pi)));

                    normals.push_back(glm::normalize(glm::vec3(_normal_x(u, v), _normal_y(u, v), _normal_z(u, v))));
                    normals.push_back(glm::normalize(glm::vec3(_normal_x(u, v1), _normal_y(u, v1), _normal_z(u, v1))));
                    normals.push_back(glm::normalize(glm::vec3(_normal_x(u1, v1), _normal_y(u1, v1), _normal_z(u1, v1))));

                    texcoords.push_back(glm::vec2((float)j / _count, 1.0f - (float)i / _count));
                    texcoords.push_back(glm::vec2((float)j / _count, 1.0f - (float)(i + 1) / _count));
                    texcoords.push_back(glm::vec2((float)(j + 1) / _count, 1.0f - (float)(i + 1) / _count));
                }
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

        _moebius_strip = std::make_shared<Mesh>();
        _moebius_strip->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        _moebius_strip->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        _moebius_strip->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        _moebius_strip->setPrimitiveType(GL_TRIANGLES);
        _moebius_strip->setVertexCount(vertices.size());

        std::cout << "Moebius_Strip is created with " << vertices.size() << " vertices\n";
    }

    MeshPtr _moebius_strip;
    MeshPtr _marker;
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;
    GLuint _sampler1;
    GLuint _sampler2;
    LightInfo _light;
    TexturePtr _Texture1;
    TexturePtr _Texture2;

    int _count;
    float _lr;
    float _phi;
    float _theta;
};

int main() {
    MoebiusStrip app;
    app.start();
    return 0;
}