#ifndef ASSIMP_REVISION_H_INC
#define ASSIMP_REVISION_H_INC

#define GitVersion 0xd8fc240
#define GitBranch "master"

#endif // ASSIMP_REVISION_H_INC
