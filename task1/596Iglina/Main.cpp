#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"

#include <iostream>
#include <vector>

/**
Направленный источник света
*/
class SampleApplication : public Application
{
public:
	MeshPtr _ground;

	TexturePtr _worldTex;

	GLuint _sampler;

    ShaderProgramPtr _shader;

    //Координаты источника света
    float _phi = 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

		_ground = makeTerrain();

		_ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

		_worldTex = loadTexture("596IglinaData1/earth_global.jpg");

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("596IglinaData1/diffuseDirectionalLight.vert", "596IglinaData1/diffuseDirectionalLight.frag");_shader = std::make_shared<ShaderProgram>("596IglinaData1/diffuseDirectionalLight.vert", "596IglinaData1/diffuseDirectionalLight.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);

		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));

                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        _shader->setVec3Uniform("light.dir", lightDir);
        _shader->setVec3Uniform("light.La", _lightAmbientColor);
        _shader->setVec3Uniform("light.Ld", _lightDiffuseColor);

		GLuint textureUnitForDiffuseTex = 0;

		if (USE_DSA) {
			glBindTextureUnit(textureUnitForDiffuseTex, _worldTex->texture());
			glBindSampler(textureUnitForDiffuseTex, _sampler);
		}
		else {
			glBindSampler(textureUnitForDiffuseTex, _sampler);
			glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
			_worldTex->bind();
		}
		_shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку

		{
			_shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
			_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));

			_shader->setVec3Uniform("material.Ka", glm::vec3(1.0, 1.0, 1.0));
			_shader->setVec3Uniform("material.Kd", glm::vec3(1.0, 1.0, 1.0));
			_shader->setIntUniform("isPolygon", 0);
			_ground->draw();
			_shader->setIntUniform("isPolygon", 1);
			_ground->drawPolygons();
		}
    }
};

int main()
{
    SampleApplication app;
	std::cout << "Works!\n";
    app.start();

    return 0;
}
