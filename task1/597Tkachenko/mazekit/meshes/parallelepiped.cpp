#include "parallelepiped.hpp"


#include <iostream>
#include <vector>

namespace mazekit {
namespace meshes {
	
namespace {

template<typename VecItem>
void appendVector(const std::vector<VecItem>& input, std::vector<VecItem>* outputVector) {
    for (auto& item : input) {
        outputVector->push_back(item);
    }
}

std::vector<glm::vec3> makeRectangleVertices(int posAxis, int widthAxis, int heightAxis,
                                             float position, float width, float height) {
    std::vector<glm::vec3> vertices;

    // Initialization order: right top, right bottom, left top, left bottom
    // Triangles: rt, rb, lt; lb, rb, lt
    glm::vec3 rightTopPoint;
    rightTopPoint[posAxis] = position;
    rightTopPoint[widthAxis] = width * 0.5f;
    rightTopPoint[heightAxis] = height * 0.5f;

    glm::vec3 rightBottomPoint;
    rightBottomPoint[posAxis] = position;
    rightBottomPoint[widthAxis] = width * 0.5f;
    rightBottomPoint[heightAxis] = -height * 0.5f;

    glm::vec3 leftTopPoint;
    leftTopPoint[posAxis] = position;
    leftTopPoint[widthAxis] = -width * 0.5f;
    leftTopPoint[heightAxis] = height * 0.5f;

    glm::vec3 leftBottomPoint;
    leftBottomPoint[posAxis] = position;
    leftBottomPoint[widthAxis] = -width * 0.5f;
    leftBottomPoint[heightAxis] = -height * 0.5f;

    // fill triangles

    vertices.push_back(rightTopPoint);
    vertices.push_back(rightBottomPoint);
    vertices.push_back(leftTopPoint);

    vertices.push_back(leftBottomPoint);
    vertices.push_back(leftTopPoint);
    vertices.push_back(rightBottomPoint);

    return vertices;
}

std::vector<glm::vec3> makeRectangleNormals(int axis, bool isPositive) 
{
    float normalValue = 1.0f;
    if (!isPositive) {
        normalValue = -1.0f;
    }
    std::vector<glm::vec3> normals;

    glm::vec3 normalPoint(0.0f, 0.0f, 0.0f);
    normalPoint[axis] = normalValue;

	normals.assign(6, normalPoint);

	return normals;
}

std::vector<glm::vec2> makeTextureCoordinates() 
{
    std::vector<glm::vec2> textureCoordinates;
    textureCoordinates.push_back(glm::vec2(1.0, 1.0));
    textureCoordinates.push_back(glm::vec2(1.0, 0.0));
    textureCoordinates.push_back(glm::vec2(0.0, 1.0));

    textureCoordinates.push_back(glm::vec2(0.0, 0.0));
    textureCoordinates.push_back(glm::vec2(0.0, 1.0));
    textureCoordinates.push_back(glm::vec2(1.0, 0.0));

    return textureCoordinates;
}
	
}

MeshPtr makeParallelepiped(float length, float width, float height)
{
	std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> textureCoordinates;

    int X_AXIS = 0;
    int Y_AXIS = 1;
    int Z_AXIS = 2;

    appendVector(makeRectangleVertices(X_AXIS, Y_AXIS, Z_AXIS, length * 0.5f, width, height), &vertices);
    appendVector(makeRectangleNormals(X_AXIS, true), &normals);
    appendVector(makeTextureCoordinates(), &textureCoordinates);

    appendVector(makeRectangleVertices(X_AXIS, Y_AXIS, Z_AXIS, -length * 0.5f, width, height), &vertices);
    appendVector(makeRectangleNormals(X_AXIS, false), &normals);
    appendVector(makeTextureCoordinates(), &textureCoordinates);

    appendVector(makeRectangleVertices(Y_AXIS, Z_AXIS, X_AXIS, width * 0.5f, height, length), &vertices);
    appendVector(makeRectangleNormals(Y_AXIS, true), &normals);
    appendVector(makeTextureCoordinates(), &textureCoordinates);

    appendVector(makeRectangleVertices(Y_AXIS, Z_AXIS, X_AXIS, -width * 0.5f, height, length), &vertices);
    appendVector(makeRectangleNormals(Y_AXIS, false), &normals);
    appendVector(makeTextureCoordinates(), &textureCoordinates);

    appendVector(makeRectangleVertices(Z_AXIS, X_AXIS, Y_AXIS, height * 0.5f, length, width), &vertices);
    appendVector(makeRectangleNormals(Z_AXIS, true), &normals);
    appendVector(makeTextureCoordinates(), &textureCoordinates);

    appendVector(makeRectangleVertices(Z_AXIS, X_AXIS, Y_AXIS, -height * 0.5f, length, width), &vertices);
    appendVector(makeRectangleNormals(Z_AXIS, false), &normals);
    appendVector(makeTextureCoordinates(), &textureCoordinates);

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(textureCoordinates.size() * sizeof(float) * 2, textureCoordinates.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}

}
}