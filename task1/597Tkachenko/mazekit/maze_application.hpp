#pragma once

#include "../common/Application.hpp"
#include "../common/Mesh.hpp"
#include "../common/ShaderProgram.hpp"

#include "maze.hpp"
#include "maze_camera_mover.hpp"

#include <iostream>


namespace mazekit {

class MazeApplication : public Application {
public:
	MazeApplication();

protected:
	void makeScene() override;
	void draw() override;
	void handleKey(int key, int scancode, int action, int mods) override;

private:
	void initFloor(float width, float height);
	void initWalls();
	void initCeil();

	glm::vec3 getWallCoordinates(int xPosition, int yPosition, bool isHorizontal);

	MeshPtr floorMesh_;
	std::vector<MeshPtr> wallMeshes_;
	MeshPtr ceilMesh_;

	ShaderProgramPtr shader_;

	mazekit::MazePtr maze_;

	CameraMoverPtr activeCamera_;
	CameraMoverPtr inactiveCamera_;

	int xSize_;
	int ySize_;
	
	bool drawCeil_ = false;
};

}