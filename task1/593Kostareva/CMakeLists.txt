cmake_minimum_required(VERSION 3.0)

set(SRC_FILES
    Main.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/PerlinNoise.cpp
    common/DebugOutput.cpp
)

SET(HEADER_FILES
    common/Application.hpp
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
    common/PerlinNoise.h
    common/DebugOutput.h
    common/Common.h
)

include_directories(common)

MAKE_OPENGL_TASK(593Kostareva 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(593Kostareva1 stdc++fs)
endif()